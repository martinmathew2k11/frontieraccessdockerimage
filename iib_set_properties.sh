#!/bin/bash
# © Copyright IBM Corporation 2015.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html

set -e

NODE_NAME=${NODENAME-IIBV10NODE}
SERVER_NAME=${SERVERNAME-default}

cd /opt/ibm/iib-10.0.0.10/server/bin
#source ./mqsiprofile
#mqsichangeproperties MYNODE -b httplistener -o HTTPConnector -n corsEnabled  -v true
#mqsichangeproperties MYNODE -b httplistener -o HTTPConnector -n corsExposeHeaders -v \"Access-Control-Allow-Origin,Content-Type,Access-Control-Allow-Headers\"
#mqsichangeproperties MYNODE -b httplistener -o HTTPConnector -n corsAllowHeaders  -v \"Access-Control-Allow-Origin,Content-Type,soapaction,Access-Control-Allow-Header\"	

mqsichangeproperties MYNODE -e default -o HTTPConnector -n corsEnabled  -v true
mqsichangeproperties MYNODE -e default -o HTTPConnector -n corsExposeHeaders -v \"Access-Control-Allow-Origin,Content-Type,Access-Control-Allow-Headers\"
mqsichangeproperties MYNODE -e default -o HTTPConnector -n corsAllowHeaders  -v \"Access-Control-Allow-Origin,Content-Type,soapaction,Access-Control-Allow-Headers\"