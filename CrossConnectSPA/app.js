
// create our angular app and inject ngAnimate and ui-router 
// =============================================================================
angular.module('formApp', ['ngAnimate', 'ui.router'])
 

 
 

// configuring our routes 
// =============================================================================
.config(function($stateProvider, $urlRouterProvider) {
    
    $stateProvider
    
        // route to show our basic form (/form)
        .state('form', {
            url: '/form',
            templateUrl: 'form.html',
            controller: 'formController'
        })
        
        // nested states 
        // each of these sections will have their own view
        // url will be nested (/form/profile)
        .state('form.objSelection', {
            url: '/objSelection',
            templateUrl: 'form-objSelection.html'
        })
        
        // url will be /form/interests
        .state('form.hsi', {
            url: '/hsi',
            templateUrl: 'form-hsi.html'
        })
        
        // url will be /form/payment
        .state('form.video', {
            url: '/video',
            templateUrl: 'form-video.html'
        })

        .state('form.voice', {
            url: '/voice',
            templateUrl: 'form-voice.html'
        })

        .state('form.review', {
            url: '/review',
            templateUrl: 'form-review.html'
        })

        .state('form.result', {
            url: '/result',
            templateUrl: 'form-result.html'
        });
       
    // catch all route
    // send users to the form page 
    $urlRouterProvider.otherwise('/form/objSelection');
})

 

// our controller for the form
// =============================================================================
.controller('formController', function($scope,$state) {
    
    // we will store all of our form data in this object
    $scope.nextPage='form.hsi';

    $scope.formData = {}; 

     $scope.reviewData = {}; 

    $scope.jsonData = {};

    $scope.hsiTemplateCtr = 0;
    $scope.hsiSavedTemplateCtr = 0;

    $scope.voiceTemplateCtr = 0;
    $scope.voiceSavedTemplateCtr = 0;
    $scope.result= false;

    $scope.provisionButtonText = "Submit";
     $scope.responseCode= '';
     $scope.responseStr= '';
 
 
  $scope.copyToReview = function() {

         $scope.reviewData.objectLocationID = $scope.formData.core.objectLocationID;
         $scope.reviewData.objectLocation = $scope.formData.core.objectLocation;
         $scope.reviewData.vendor = $scope.formData.core.vendor;
         $scope.reviewData.hardwareType = $scope.formData.core.hardwareType;
         $scope.reviewData.dwellingType = $scope.formData.core.dwellingType;
         $scope.reviewData.wiringType = $scope.formData.core.wiringType;
         $scope.reviewData.serviceSelections = $scope.formData.core.services;
         $scope.reviewData.ontSerialNumber = $scope.formData.core.ontSerialNumber;

         $scope.reviewData.environment = $scope.formData.core.environment;
         $scope.reviewData.operation = $scope.formData.core.operation;
         $scope.reviewData.ontType = $scope.formData.core.ontType;
         $scope.reviewData.service = $scope.formData.core.service;


         if($scope.reviewData.dwellingType === 'GSFU'){
            $scope.reviewData.serviceConfigurations = $scope.formData.serviceConfigurations;
        }
        else
        {
            $scope.reviewData.serviceConfigurations = {};
            $scope.reviewData.serviceConfigurations.NokiaServiceConfiguration = {};

            if($scope.formData.serviceConfigurations.NokiaServiceConfiguration)
            {

                if($scope.formData.serviceConfigurations.NokiaServiceConfiguration.VideoServices)
                {
                    $scope.reviewData.serviceConfigurations.NokiaServiceConfiguration.VideoServices = {}; 
                    $scope.reviewData.serviceConfigurations.NokiaServiceConfiguration.VideoServices = 
                                jQuery.extend(true,{},$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VideoServices);
                }

                 if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices)
                {
                    $scope.reviewData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices = {}; 
                    $scope.reviewData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices = 
                          jQuery.extend(true,{},$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices);
                }

                 if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices)
                {
                    $scope.reviewData.serviceConfigurations.NokiaServiceConfiguration.HSIServices = {}; 
                      $scope.reviewData.serviceConfigurations.NokiaServiceConfiguration.HSIServices = 
                          jQuery.extend(true,{},$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices);
                }
            }

             
 
        }

       

  };

  $scope.goForward = function() {

    $scope.copyToReview();



    if($state.current.name === 'form.objSelection')
        {
          if($scope.formData.core.operation === 'CLEAN_ADD_SERVICES' || $scope.formData.core.operation === 'ADD_SERVICE')
          {
            if($scope.formData.core.services.serviceHSI)
              $state.go('form.hsi');
            else if(!$scope.formData.core.services.serviceHSI && $scope.formData.core.services.serviceVideo)
              $state.go('form.video');
            else if(!$scope.formData.core.services.serviceHSI && !$scope.formData.core.services.serviceVideo && $scope.formData.core.services.serviceVoice)
              $state.go('form.voice');
          }
          else
          {
              $state.go('form.review');
          }

        }

    if($state.current.name === 'form.hsi')
        {           
          if($scope.formData.core.services.serviceVideo)
            $state.go('form.video');
          else if(!$scope.formData.core.services.serviceVideo && $scope.formData.core.services.serviceVoice)
            $state.go('form.voice');
          else
            $state.go('form.review');
        }

    if($state.current.name === 'form.video')
        {           
          if($scope.formData.core.services.serviceVoice)
            $state.go('form.voice');
          else
            $state.go('form.review');
        }

    if($state.current.name === 'form.voice')
        {            
            $state.go('form.review');
        }

  };

  $scope.goBack = function() {

    $scope.copyToReview();


    if($state.current.name === 'form.review')
        {
          if($scope.formData.core.operation === 'CLEAN_ADD_SERVICES' || $scope.formData.core.operation === 'ADD_SERVICE')
          {
            if($scope.formData.core.services.serviceVoice)
              $state.go('form.voice');
            else if(!$scope.formData.core.services.serviceVoice && $scope.formData.core.services.serviceVideo)
              $state.go('form.video');
            else if(!$scope.formData.core.services.serviceVoice && !$scope.formData.core.services.serviceVideo && $scope.formData.core.services.serviceHSI)
              $state.go('form.hsi');
            else
              $state.go('form.objSelection');
          }
          else
          {
              $state.go('form.objSelection');
          }
        }

    if($state.current.name === 'form.voice')
        {           
          if($scope.formData.core.services.serviceVideo)
            $state.go('form.video');
          else if(!$scope.formData.core.services.serviceVideo && $scope.formData.core.services.serviceHSI)
            $state.go('form.hsi');
          else
            $state.go('form.objSelection');
        }
 
    if($state.current.name === 'form.video')
        {           
          if($scope.formData.core.services.serviceHSI)
            $state.go('form.hsi');
          else
            $state.go('form.objSelection');
        }

    if($state.current.name === 'form.hsi')
        {            
            $state.go('form.objSelection');
        }

  };


    
    // function to process the form
    $scope.processForm = function() {

       

         $scope.jsonData.objectLocationID = $scope.formData.core.objectLocationID;
         $scope.jsonData.objectLocation = $scope.formData.core.objectLocation;
         $scope.jsonData.vendor = $scope.formData.core.vendor;
         $scope.jsonData.hardwareType = $scope.formData.core.hardwareType;
         $scope.jsonData.dwellingType = $scope.formData.core.dwellingType;
         $scope.jsonData.wiringType = $scope.formData.core.wiringType;
         $scope.jsonData.serviceSelections = $scope.formData.core.services;

          $scope.jsonData.environment = $scope.formData.core.environment;
         $scope.jsonData.operation = $scope.formData.core.operation;
         $scope.jsonData.serviceSelection = $scope.formData.core.service;

         $scope.provisionButtonText = "Working";
         $scope.isDisabled = true;

         if($scope.jsonData.dwellingType === 'GSFU'){
            $scope.jsonData.serviceConfigurations = $scope.formData.serviceConfigurations;
        }
        else
        {

              if($scope.formData.serviceConfigurations.NokiaServiceConfiguration)
            {
                if($scope.formData.serviceConfigurations.NokiaServiceConfiguration.VideoServices)
                {
                     if($scope.formData.serviceConfigurations.NokiaServiceConfiguration.VideoServices.Nokia_7342_Video_Template)
                        { 
 
                             if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration === undefined)
                             {
                                 $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration = {};
                             }
                         
                            $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VideoServices= {};
                            $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VideoServices.Nokia_7342_Video_Template= {};

                            $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VideoServices.Nokia_7342_Video_Template =
                                $scope.formData.serviceConfigurations.NokiaServiceConfiguration.VideoServices.Nokia_7342_Video_Template;
                        }
                }
            }
        }

        

         
 
         //added for json to xml
         var feedback=''; 

         var retCode='';
  

        var ajaxReturn = $.soap({
            url: 'http://localhost:7800/CrossConnect',
            method: 'CrossConnectProvisioning',

            appendMethodToURL: false,
            SOAPAction: 'frontier/fac/NewOperation',
            soap12: false,
            async: true,
            withCredentials: false,

            data: $scope.jsonData,
            wss: '',

            HTTPHeaders: {
                            "Access-Control-Allow-Origin":"*",
                            "Access-Control-Allow-Headers":"*"
                        },
            envAttributes: '',
            SOAPHeader: '',

            namespaceQualifier:  'myns',
            namespaceURL: 'frontier/fac',
            noPrefix: true,
            elementName: '',

            enableLogging: true,

            context: feedback,

            beforeSend: function(SOAPEnvelope) {
                var xmlout = dom2html($.parseXML(SOAPEnvelope.toString()).firstChild);
               //feedback = xmlout; 
                feedback = SOAPEnvelope.toString(); 
                //alert('Provisioning payload for FAC:\n'+feedback);
            },
            success: function(SOAPResponse) {
                feedback = dom2html(SOAPResponse.toXML()); 
                $scope.responseStr= feedback;

                if(feedback.includes("SUCCESS"))
                {
                    //alert('Provisioning SUCCESS');
                    $scope.isDisabled = false;
                    $scope.provisionButtonText = "Submit";
                     $scope.result= true;
                     $scope.responseStr= 'SUCCESS';
                      $state.go('form.result');
                      
                }
                else
                {
                    //alert('Provisioning FAILURE');
                    $scope.isDisabled = false;
                    $scope.provisionButtonText = "Submit";
                     $scope.result= false;
                     $scope.responseStr= 'FAILURE';
                      $state.go('form.result');

                }
            },
            error: function(SOAPResponse) {
                feedback = SOAPResponse.toString(); 
                $scope.responseStr= feedback;
                 //feedback = 'Error'; 
                 //alert('Interfacing Error:\n'+feedback);
                 $scope.isDisabled = false;
                 $scope.provisionButtonText = "Submit";
                  $scope.result= false;
                  $scope.responseStr= 'ERROR';
                   $state.go('form.result');
            },
            statusCode: {
                404: function() {
                    console.log('404')
                    retCode = '404';
                    $scope.responseCode= retCode;
                },
                200: function() {
                    console.log('200')
                    retCode = '200';
                     $scope.responseCode= retCode;
                }
            }
        });

        //end conversion
        
         
    };

    

function dom2html(xmldom, tabcount) {
    var whitespace = /^\s+$/;
    var tabs = '  ';
    var xmlout = [];
    tabcount = (tabcount) ? tabcount : 0;

    xmlout.push('\n', repeat(tabs, tabcount), '<', xmldom.nodeName);
    for (var i in xmldom.attributes) {
        var attribute = xmldom.attributes[i];
        if (attribute.nodeType === 2) {
            xmlout.push(' ', attribute.name, '="', attribute.value, '"');
        }
    }
    xmlout.push('>');
    ++tabcount;
    // for (var j in xmldom.childNodes) {
    for (var j = 0; j < xmldom.childNodes.length; j++) {
        var child = xmldom.childNodes[j];
        if (child.nodeType === 1) {
            xmlout.push(dom2html(child, tabcount));
        }
        if (child.nodeType === 3 && !whitespace.test(child.nodeValue)) {
            xmlout.push(child.nodeValue.trim());
        }
        if (child.nodeType === 4) {
            xmlout.push('<![CDATA[' + child.nodeValue + ']]>');
        }
    }
    if (xmldom.childNodes.length === 1 && (xmldom.childNodes[0].nodeType === 3 || xmldom.childNodes[0].nodeType === 4)) {
        xmlout.push('</', xmldom.nodeName, '>');
    } else {
        xmlout.push('\n', repeat(tabs, --tabcount),'</', xmldom.nodeName, '>');
    }
    return xmlout.join('');
}

function repeat(x, n) {
    var s = '';
    for (;;) {
        if (n & 1) s += x;
        n >>= 1;
        if (n) x += x;
        else break;
    }
    return s;
}


      $scope.reset = function() { 
        $scope.formData = {}; 
        $scope.jsonData = {};

        $scope.reviewData = {};

        $scope.hsiTemplateCtr = 0;
       $scope.hsiSavedTemplateCtr = 0;

         $scope.voiceTemplateCtr = 0;
       $scope.voiceSavedTemplateCtr = 0;

    };

      $scope.resetServiceConfig = function() { 
       $scope.hsiTemplateCtr = 0;
       $scope.hsiSavedTemplateCtr = 0;

        $scope.voiceTemplateCtr = 0;
       $scope.voiceSavedTemplateCtr = 0;

       $scope.jsonData.serviceConfigurations = {};
       $scope.formData.serviceConfigurations = {};
       $scope.reviewData.serviceConfigurations = {};
 
    };

     $scope.resetHSI = function() { 
       $scope.hsiTemplateCtr = 0;
       $scope.hsiSavedTemplateCtr = 0;

       if($scope.formData.core.dwellingType=='GMDU')
       {
            if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration)
            {
                if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices)
                {
                     
                        delete $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices; 
                        delete $scope.formData.serviceConfigurations.NokiaServiceConfiguration.HSIServices;

                        delete $scope.reviewData.serviceConfigurations.NokiaServiceConfiguration.HSIServices;
                }
            }
        }

        if($scope.formData.core.dwellingType=='GSFU')
        {
            if($scope.formData.serviceConfigurations.NokiaServiceConfiguration)
            {
                if($scope.formData.serviceConfigurations.NokiaServiceConfiguration.HSIServices)
                {
                     delete $scope.formData.serviceConfigurations.NokiaServiceConfiguration.HSIServices;

                     delete $scope.reviewData.serviceConfigurations.NokiaServiceConfiguration.HSIServices;
                }
            }
        }
 
 
    };

    $scope.resetVideo = function() {  
   

        if($scope.formData.serviceConfigurations.NokiaServiceConfiguration)
            {
                if($scope.formData.serviceConfigurations.NokiaServiceConfiguration.VideoServices)
                {
                     delete $scope.formData.serviceConfigurations.NokiaServiceConfiguration.VideoServices;

                     delete $scope.reviewData.serviceConfigurations.NokiaServiceConfiguration.VideoServices;
                }
            }
 
    };

    $scope.resetVoice = function() { 
         $scope.voiceTemplateCtr = 0;
       $scope.voiceSavedTemplateCtr = 0;
 
 

        if($scope.formData.core.dwellingType=='GMDU')
       {
            if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration)
            {
                if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices)
                {
                     
                        delete $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices; 
                        delete $scope.formData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices;

                        delete $scope.reviewData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices;
                }
            }
        }

        if($scope.formData.core.dwellingType=='GSFU')
        {
            if($scope.formData.serviceConfigurations.NokiaServiceConfiguration)
            {
                if($scope.formData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices)
                {
                     delete $scope.formData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices;

                     delete $scope.reviewData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices;
                }
            }
        }
 
    };


    



     $scope.addHSI = function() { 

        if($scope.formData.core.wiringType=='ETHERNET')
        {
            if($scope.hsiSavedTemplateCtr > 0)
            {
                 if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template[$scope.hsiTemplateCtr]){
                
                     $scope.hsiTemplateCtr = $scope.hsiTemplateCtr + 1;
                     $scope.formData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template = {};
                }
            }
        }

        

        if($scope.formData.core.wiringType=='VDSL2')
        {
            if($scope.hsiSavedTemplateCtr > 0)
            {
                 if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template[$scope.hsiTemplateCtr]){
                
                     $scope.hsiTemplateCtr = $scope.hsiTemplateCtr + 1;
                     $scope.formData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template = {};
                }
            }
        } 
    };

    
     $scope.saveHSI = function() { 

        if($scope.formData.core.wiringType=='ETHERNET')
        {

             if($scope.hsiSavedTemplateCtr ===0){
               
                 if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration === undefined)
                 {
                     $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration = {};
                 }
                  
                     $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices= {};
                     $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template= [];
                  
                }
     
                if(!$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template[$scope.hsiTemplateCtr]){

                     $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template[$scope.hsiTemplateCtr] = {};
                     $scope.hsiSavedTemplateCtr = $scope.hsiSavedTemplateCtr + 1;
                }
                  
                $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template[$scope.hsiTemplateCtr] = 
                    $scope.formData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template;
         

                $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template[$scope.hsiTemplateCtr].ontSerialNumber = 
                    $scope.formData.core.ontSerialNumber;
        }

        if($scope.formData.core.wiringType=='VDSL2')
        {

             if($scope.hsiSavedTemplateCtr ===0){
               
                 if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration === undefined)
                 {
                     $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration = {};
                 }
                  
                     $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices= {};
                     $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template= [];
                  
                }
     
                if(!$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template[$scope.hsiTemplateCtr]){

                     $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template[$scope.hsiTemplateCtr] = {};
                     $scope.hsiSavedTemplateCtr = $scope.hsiSavedTemplateCtr + 1;
                }
                 

                $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template[$scope.hsiTemplateCtr] = 
                    $scope.formData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template;
         

                $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template[$scope.hsiTemplateCtr].ontSerialNumber = 
                    $scope.formData.core.ontSerialNumber;
        }
    };
 

    $scope.deleteHSI = function() { 

        if($scope.formData.core.wiringType=='ETHERNET')
        {

             if($scope.hsiTemplateCtr > 0 && $scope.hsiSavedTemplateCtr > $scope.hsiTemplateCtr){

                 if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template[$scope.hsiTemplateCtr]){

                    $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template.splice($scope.hsiTemplateCtr,1);
                    $scope.hsiSavedTemplateCtr = $scope.hsiSavedTemplateCtr -1;
                    $scope.hsiTemplateCtr = $scope.hsiTemplateCtr -1;

                     $scope.formData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template = 
                            jQuery.extend({},$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template[$scope.hsiTemplateCtr]);

                 }

             }
        }

        if($scope.formData.core.wiringType=='VDSL2')
        {

             if($scope.hsiTemplateCtr > 0 && $scope.hsiSavedTemplateCtr > $scope.hsiTemplateCtr){

                 if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template[$scope.hsiTemplateCtr]){

                    $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template.splice($scope.hsiTemplateCtr,1);
                    $scope.hsiSavedTemplateCtr = $scope.hsiSavedTemplateCtr -1;
                    $scope.hsiTemplateCtr = $scope.hsiTemplateCtr -1;

                     $scope.formData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template = 
                            jQuery.extend({},$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template[$scope.hsiTemplateCtr]);

                 }

             }
        }

    };

    $scope.prevHSI = function() { 

        if($scope.formData.core.wiringType=='ETHERNET')
        {
            if($scope.hsiTemplateCtr > 0 && $scope.hsiSavedTemplateCtr > 0){

                if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template[$scope.hsiTemplateCtr - 1]){

                    $scope.hsiTemplateCtr = $scope.hsiTemplateCtr - 1;

                        $scope.formData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template = 
                            jQuery.extend({},$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template[$scope.hsiTemplateCtr]);
     
                }
            }
        }

         if($scope.formData.core.wiringType=='VDSL2')
        {
            if($scope.hsiTemplateCtr > 0 && $scope.hsiSavedTemplateCtr > 0){

                if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template[$scope.hsiTemplateCtr - 1]){

                    $scope.hsiTemplateCtr = $scope.hsiTemplateCtr - 1;

                        $scope.formData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template = 
                            jQuery.extend({},$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template[$scope.hsiTemplateCtr]);
     
                }
            }
        }

    };

    $scope.copyHSI = function() {  

        if($scope.formData.core.wiringType=='ETHERNET')
        {
            if($scope.hsiTemplateCtr > 0 && $scope.hsiSavedTemplateCtr > 0){
     
                if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template[$scope.hsiTemplateCtr - 1]){

                          $scope.formData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template = 
                           jQuery.extend({},$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template[$scope.hsiTemplateCtr - 1]);
 
                }

            }
        }

        if($scope.formData.core.wiringType=='VDSL2')
        {
            if($scope.hsiTemplateCtr > 0 && $scope.hsiSavedTemplateCtr > 0){
     
                if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template[$scope.hsiTemplateCtr - 1]){
      
                        $scope.formData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template = 
                            jQuery.extend({},$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template[$scope.hsiTemplateCtr - 1]);

                }

            }
        } 
    };

    $scope.nextHSI = function() {  

        if($scope.formData.core.wiringType=='ETHERNET')
        {
            if($scope.hsiSavedTemplateCtr > 0){

                if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template[$scope.hsiTemplateCtr + 1]){

                    $scope.hsiTemplateCtr = $scope.hsiTemplateCtr + 1;

                        $scope.formData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template = 
                            jQuery.extend({},$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_ETH_Template[$scope.hsiTemplateCtr]);
                        
                }
            }
        }

        if($scope.formData.core.wiringType=='VDSL2')
        {
            if($scope.hsiSavedTemplateCtr > 0){

                if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template[$scope.hsiTemplateCtr + 1]){

                    $scope.hsiTemplateCtr = $scope.hsiTemplateCtr + 1;

                        $scope.formData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template = 
                            jQuery.extend({},$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.HSIServices.Nokia_7342_VDSL2_Template[$scope.hsiTemplateCtr]);
                        
                }
            }
        }
    };

    //voice functions

      $scope.addVoice = function() { 

        if($scope.voiceSavedTemplateCtr > 0)
        {
             if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template[$scope.voiceTemplateCtr]){
            
                 $scope.voiceTemplateCtr = $scope.voiceTemplateCtr + 1;
                 $scope.formData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template = {};
            }
        }
          
    };

    
     $scope.saveVoice = function() { 

         if($scope.voiceSavedTemplateCtr ===0){
           
             if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration === undefined)
             {
                 $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration = {};
             }
              
                 $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices= {};
                 $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template= [];
              
            }
 
            if(!$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template[$scope.voiceTemplateCtr]){

                 $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template[$scope.voiceTemplateCtr] = {};
                 $scope.voiceSavedTemplateCtr = $scope.voiceSavedTemplateCtr + 1;
            }
             

            $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template[$scope.voiceTemplateCtr] = 
                $scope.formData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template;
     

            $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template[$scope.voiceTemplateCtr].ontSerialNumber = 
                $scope.formData.core.ontSerialNumber;
             
    };
 

    $scope.deleteVoice = function() { 

         if($scope.voiceTemplateCtr > 0 && $scope.voiceSavedTemplateCtr > $scope.voiceTemplateCtr){

             if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template[$scope.voiceTemplateCtr]){

                $scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template.splice($scope.voiceTemplateCtr,1);
                $scope.voiceSavedTemplateCtr = $scope.voiceSavedTemplateCtr -1;
                $scope.voiceTemplateCtr = $scope.voiceTemplateCtr -1;

                 $scope.formData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template = 
                        jQuery.extend({},$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template[$scope.voiceTemplateCtr]);

             }

         }

    };

    $scope.prevVoice = function() { 
        if($scope.voiceTemplateCtr > 0 && $scope.voiceSavedTemplateCtr > 0){

            if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template[$scope.voiceTemplateCtr - 1]){

                $scope.voiceTemplateCtr = $scope.voiceTemplateCtr - 1;

                    $scope.formData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template = 
                        jQuery.extend({},$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template[$scope.voiceTemplateCtr]);
 
            }

        }
    };

    $scope.copyVoice = function() {  

        if($scope.voiceTemplateCtr > 0 && $scope.voiceSavedTemplateCtr > 0){
 
            if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template[$scope.voiceTemplateCtr - 1]){
  
                    $scope.formData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template = 
                        jQuery.extend({},$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template[$scope.voiceTemplateCtr - 1]);

            }

        }
    };

    $scope.nextVoice = function() {  

        if($scope.voiceSavedTemplateCtr > 0){

            if($scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template[$scope.voiceTemplateCtr + 1]){

                $scope.voiceTemplateCtr = $scope.voiceTemplateCtr + 1;

                    $scope.formData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template = 
                        jQuery.extend({},$scope.jsonData.serviceConfigurations.NokiaServiceConfiguration.VoiceServices.Nokia_7342_Voice_Template[$scope.voiceTemplateCtr]);
                    
            }

        }
    };


     

     $scope.validateObjectForm = function() {   

        if($scope.formData.core.services.serviceHSI == "'YES'" || $scope.formData.core.services.serviceVideo == "'YES'" ||
            $scope.formData.core.services.serviceVoice == "'YES'" ){

            return true;
        }

        

     };

      $scope.getNextObjectForm = function() {   

        
        if($scope.formData.core.services.serviceHSI)
        {
            $scope.nextPage='form.hsi';
            return $scope.nextPage;
        }

        if($scope.formData.core.services.serviceVideo)
        {
            $scope.nextPage='form.video'; 
            return $scope.nextPage;
        }

        if($scope.formData.core.services.serviceVoice)
        {
            $scope.nextPage='form.voice'; 
            
        }

         

        return $scope.nextPage;
        

     };
 
 
});


 

