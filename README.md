### To create the image for frontier access ###
 
 * docker build -t frontier_access .

### To run the container with the image ###

* docker run -d -p 8080:8080 -p 4414:4414 -p 7800:7800 --name myNode -e LICENSE=accept -e NODENAME=MYNODE frontier_access

### To check if the portal is up and runnning in the container ###

* curl http://localhost:8080
## If you are running using the docker tool box ##
* Find the IP of the container using 'docker-machine ls'
  then do a curl http://IP:8080/
### To deploy the application to the Integration BUS ###
* mqsideploy.exe -i IP_INTEGRATION_SERVER -p port(default:4414) -e SERVER_NAME -a PATH_TO_BAR_FILE

